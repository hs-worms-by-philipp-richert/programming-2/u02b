#pragma once
#include <string>

using namespace std;

class cFunkMast
{
private:
	int antenna;
	double range;
	double height;
	double latitude;
	double longitude;
	bool validateHeight(int, double);
public:
	cFunkMast(int = 0, double = 0.0, double = 0.0, double = 49.7, double = 8.3);
	void eingabe();
	void ausgabe(bool);
};

