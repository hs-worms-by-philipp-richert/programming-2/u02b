// Aufgabe u02b
// Uebungsaufgabe Array aus Objekten
// Philipp Richert
// 20.10.2022

#include "cFunkMast.h"
#include <iostream>

using namespace std;

int main() {
	try
	{
		cFunkMast mast[100];

		// get user input for 5 elements
		int i = 0;
		while (i < 5)
		{
			mast[i].eingabe();
			i++;
		}

		// output all elements
		int size = sizeof(mast) / sizeof(*mast);
		for (int o = 0; o < size; o++) {
			bool firstEl = (o == 0) ? true : false;	// is first element?
			mast[o].ausgabe(firstEl);
		}
	}
	catch (const std::exception& e) // catch any errors
	{
		cout << "Fehler - " << e.what() << endl;
	}

	return 0;
}