#include "cFunkMast.h"
#include <iostream>
#include <iomanip>

// validate tower height
// allowed values are:
//	-up to 4 antennas and less than 30m
//	-more than 4 antennas and less than 50m
bool cFunkMast::validateHeight(int antenna, double height) {
	if (antenna <= 4 && height > 30.0) return false;
	else if (antenna > 4 && height > 50.0) return false;
	return true;
}

// class constructor
cFunkMast::cFunkMast(int ant_in, double range_in, double height_in, double lat_in, double long_in) {
	// validate height
	if (!cFunkMast::validateHeight(ant_in, height_in)) {
		throw invalid_argument(to_string(height_in) + " Meter ist keine zulaessige Hoehe fuer " + to_string(ant_in) + " Antennen");
	}

	antenna = ant_in;
	range = range_in;
	height = height_in;
	latitude = lat_in;
	longitude = long_in;
}

// eingabe method
// gets user input for current object
void cFunkMast::eingabe() {
	int tmpAnt;
	double tmpRange;
	double tmpHeight;
	double tmpLat;
	double tmpLong;
	bool done = false;

	while (!done)
	{
		cout << "Neuen Funkmast anlegen:" << endl
			<< "\tAntennenzahl (int): ";
		cin >> tmpAnt;

		cout << "\tReichweite (double): ";
		cin >> tmpRange;

		cout << "\tHoehe (double): ";
		cin >> tmpHeight;

		cout << "\tBreitengrad (double): ";
		cin >> tmpLat;

		cout << "\tLaengengrad (double):";
		cin >> tmpLong;

		// validate height
		if (!cFunkMast::validateHeight(tmpAnt, tmpHeight)) {
			cout << endl << to_string(tmpHeight) << " Meter ist keine zulaessige Hoehe fuer " << to_string(tmpAnt) << " Antennen" << endl
				<< endl << "Bitte geben Sie die Daten erneut ein:" << endl << endl;
			continue;
		}
		
		done = true;

		// save data to current object
		antenna = tmpAnt;
		range = tmpRange;
		height = tmpHeight;
		latitude = tmpLat;
		longitude = tmpLong;
	}
}

// ausgabe methodd
// outputs current object
// prepends table header when "firstElement" is true
void cFunkMast::ausgabe(bool firstElement) {
	// table head
	if (firstElement) {
		cout << endl << "Uebersicht der Funkmaeste:" << endl;
		cout << endl << "  +----------------+--------------+---------------+------------------------------+" << endl
			<< "  | Antennenanzahl |   Leistung   |     Hoehe     |           Position           |" << endl
			<< "  +----------------+--------------+---------------+------------------------------+" << endl;
	}

	// skip when there are no antennas
	if (antenna == 0) return;

	// data row
	cout << "  | " << left << setw(14) << to_string(antenna)
		<< " | " << left << setw(12) << to_string(range) + " m"
		<< " | " << left << setw(13) << to_string(height) + " m"
		<< " | " << left << setw(28) << to_string(latitude) + "\370, " + to_string(longitude) + '\370' << " |" << endl;

	cout << "  +----------------+--------------+---------------+------------------------------+" << endl;
}
